Source: gnucobol3
Section: devel
Priority: optional
Maintainer: Thorsten Alteholz <debian@alteholz.de>
Build-Depends:
	debhelper-compat (= 13)
	, libgmp-dev
	, libdb-dev
	, libncurses5-dev
	, libxml2-dev
	, libjson-c-dev
	, quilt
	, texinfo
	, texlive
	, help2man
Standards-Version: 4.6.0
Homepage: https://www.gnu.org/software/gnucobol/
Vcs-Git: https://salsa.debian.org/alteholz/gnucobol3.git
Vcs-Browser: https://salsa.debian.org/alteholz/gnucobol3
Rules-Requires-Root: no

Package: gnucobol3
Architecture: any
Depends:
	${shlibs:Depends}
	, ${misc:Depends}
	, libcob4-dev (=${binary:Version})
	, libgmp-dev
	, libncurses5-dev
	, gcc
Breaks: open-cobol (<< 2.2), gnucobol4
Replaces: open-cobol (<< 2.2), gnucobol4
Description: COBOL compiler
 GnuCOBOL (formerly OpenCOBOL) is a free, modern COBOL compiler. GnuCOBOL
 implements a substantial part of the COBOL 85, COBOL 2002 and COBOL 2014
 standards and X/Open COBOL, as well as many extensions included in other COBOL
 compilers (IBM COBOL, MicroFocus COBOL, ACUCOBOL-GT and others).
 .
 GnuCOBOL translates COBOL into C and compiles the translated code using a
 native C compiler.
 .
 Build COBOL programs on various platforms, including GNU/Linux, Unix, Mac OS X,
 and Microsoft Windows. GnuCOBOL has also been built on HP/UX, z/OS, SPARC,
 RS6000, AS/400, along with other combinations of machines and operating
 systems.
 .
 While being held to a high level of quality and robustness, GnuCOBOL does not
 claim to be a “Standard Conforming” implementation of COBOL.
 .
 GnuCOBOL passes over 9600 of the NIST COBOL 85 test suite tests and over 750
 internal checks during build.

Package: libcob4
Section: libs
Replaces: libcob1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: COBOL compiler - runtime library
 This package contains the runtime library for gnucobol.
 .
 GnuCOBOL (formerly OpenCOBOL) is a free, modern COBOL compiler. GnuCOBOL
 implements a substantial part of the COBOL 85, COBOL 2002 and COBOL 2014
 standards and X/Open COBOL, as well as many extensions included in other COBOL
 compilers (IBM COBOL, MicroFocus COBOL, ACUCOBOL-GT and others).

Package: libcob4-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Replaces: libcob1-dev
Breaks: libcob1-dev
Depends: ${misc:Depends}, libcob4 (=${binary:Version})
Description: COBOL compiler - development files
 This package contains the development files for gnucobol.
 .
 GnuCOBOL (formerly OpenCOBOL) is a free, modern COBOL compiler. GnuCOBOL
 implements a substantial part of the COBOL 85, COBOL 2002 and COBOL 2014
 standards and X/Open COBOL, as well as many extensions included in other COBOL
 compilers (IBM COBOL, MicroFocus COBOL, ACUCOBOL-GT and others).
